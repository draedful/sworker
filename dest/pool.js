'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _packet = require('./packet.js');

var _packet2 = _interopRequireDefault(_packet);

var _isObject = require('lodash/isObject');

var _isObject2 = _interopRequireDefault(_isObject);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    pool: new Map(),
    index: 1,

    createPacket: function createPacket(method, data) {
        var currentIndex = this.index++,

        /**
         * @type {JSON-RPC~Request}
         * */
        packetData = {
            method: method,
            params: data,
            id: currentIndex
        };
        var packet = new _packet2.default(packetData);
        this.pool.set(currentIndex, packet);
        return packet;
    },
    resolve: function resolve(data) {
        if ((0, _isObject2.default)(data)) {
            if (data.id) {
                var packet = this.pool.get(data.id);
                if (packet && packet instanceof _packet2.default) {
                    packet.resolve(data);
                    this.pool.delete(data.id);
                    return true;
                }
            }
        }
        return false;
    }
};