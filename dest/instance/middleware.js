'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = require('lodash');

var _koaCompose = require('koa-compose');

var _koaCompose2 = _interopRequireDefault(_koaCompose);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Middleware = function () {
    function Middleware(middlewares) {
        _classCallCheck(this, Middleware);

        this.middlewars = [].concat(_toConsumableArray(middlewares));
    }

    _createClass(Middleware, [{
        key: 'getMiddleware',
        value: function getMiddleware() {
            return this.middlewars;
        }
    }, {
        key: 'call',
        value: function call(ctx) {
            if (this.middlewars.length) {
                return _co2.default.wrap((0, _koaCompose2.default)(this.middlewars)).call(ctx);
            }
            return Promise.reject('Method not found');
        }
    }]);

    return Middleware;
}();

exports.default = Middleware;