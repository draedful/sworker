'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _middleware = require('./middleware.js');

var _middleware2 = _interopRequireDefault(_middleware);

var _port_pool = require('./port/port_pool.js');

var _port_pool2 = _interopRequireDefault(_port_pool);

var _isFunction = require('lodash/isFunction');

var _isFunction2 = _interopRequireDefault(_isFunction);

var _koaCompose = require('koa-compose');

var _koaCompose2 = _interopRequireDefault(_koaCompose);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    middlewares: [],
    use: function use(gen) {
        if ((0, _isFunction2.default)(gen)) {
            this.middlewares.push(gen);
        }
        return this;
    },
    compose: function compose() {
        return _co2.default.wrap((0, _koaCompose2.default)(this.middlewares));
    }
};