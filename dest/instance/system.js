'use strict';

var _app = require('./app.js');

var _app2 = _interopRequireDefault(_app);

var _port_pool = require('./port/port_pool.js');

var _port_pool2 = _interopRequireDefault(_port_pool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_app2.default.use('close', function* () {
    if (!_port_pool2.default.length) {
        self.close();
    }
});