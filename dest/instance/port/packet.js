'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = require('lodash');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @typedef {object} JSON-RPC~Response
 * @property {*} result - ответ
 * @property {*} error - ошибка произошедшая при запросе
 * @property {number} id - идентификатор запроса, выданный при инициализации запроса
 * */

var Packet = function () {
    function Packet(message) {
        _classCallCheck(this, Packet);

        this.response = {};
        if ((0, _lodash.isObject)(message)) {
            this.body = message.data;
            this.method = message.method;
            this.id = message.id;
        }
    }

    _createClass(Packet, [{
        key: 'set',
        value: function set(name, value) {
            if ((0, _lodash.isObject)(name)) {
                (0, _lodash.merge)(this.response, name);
            } else {
                this.response[name] = value;
            }

            return this;
        }
    }, {
        key: 'get',
        value: function get(name) {
            return this.response[name];
        }

        /**
         * @return
         * */

    }, {
        key: 'getResponse',
        value: function getResponse() {
            var response = {
                id: this.id
            };

            if (this.error) {
                response.error = this.error;
            } else {
                response.result = this.response;
            }

            return JSON.parse(JSON.stringify(response));
        }
    }, {
        key: 'setError',
        value: function setError(err) {
            this.error = err;
        }
    }]);

    return Packet;
}();

exports.default = Packet;