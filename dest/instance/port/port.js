'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _app = require('../app.js');

var _app2 = _interopRequireDefault(_app);

var _packet = require('./packet.js');

var _packet2 = _interopRequireDefault(_packet);

var _port_pool = require('./port_pool.js');

var _port_pool2 = _interopRequireDefault(_port_pool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Port = function () {
    function Port(port) {
        var _this = this;

        _classCallCheck(this, Port);

        console.log(port);
        this.port = port;
        this.port.onmessage = function (message) {
            var packet = new _packet2.default(message.data);

            packet.port = _this;

            _app2.default.compose().call(packet).then(function () {
                _this.post(packet.getResponse());
            }).catch(function (err) {
                debugger;
                packet.setError(err);
                _this.post(packet.getResponse());
            });
        };
    }

    _createClass(Port, [{
        key: 'destroy',
        value: function destroy() {
            _port_pool2.default.remove(this);
        }
    }, {
        key: 'post',
        value: function post(message) {
            this.port.postMessage(message);
        }
    }]);

    return Port;
}();

exports.default = Port;