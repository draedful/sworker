'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _port = require('./port');

var _port2 = _interopRequireDefault(_port);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PortsPool = {

    get length() {
        return this.ports.size;
    },

    /**
     * @type {Map}
     * */
    ports: new Map(),

    /**
     * @param {MessagePort} port
     * @return {Port} - инстанс порта
     * */
    add: function add(port) {
        var portInstance = new _port2.default(port);
        this.ports.set(port, portInstance);
        return portInstance;
    },
    broadcast: function broadcast(method, data) {
        if (this.length) {
            var ports = Array.from(this.ports.values()),
                message = {
                method: method,
                data: data
            };
            each(ports, function (port) {
                port.post(message);
            });
        }
    }
};

self.onconnect = function (e) {
    var port = PortsPool.add(e.ports[0]);
    port.post({ method: 'connect' });
};

exports.default = PortsPool;