'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _isFunction = require('lodash/isFunction');

var _isFunction2 = _interopRequireDefault(_isFunction);

var _filter = require('lodash/filter');

var _filter2 = _interopRequireDefault(_filter);

var _koaCompose = require('koa-compose');

var _koaCompose2 = _interopRequireDefault(_koaCompose);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    paths: new Map(),
    use: function use(name) {
        for (var _len = arguments.length, cbs = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            cbs[_key - 1] = arguments[_key];
        }

        if (name && cbs.length) {
            if (this.paths.has(name)) {
                var middlewares = this.paths.get(name);
                Array.prototype.concat.call(middlewares, (0, _filter2.default)(cbs, _isFunction2.default));
            } else {
                this.paths.set(name, cbs);
            }
            // слияние массива без потери ссылки
        }
        return this;
    },
    has: function has(name) {
        return this.paths.has(name);
    },
    compose: function compose(name) {
        return _co2.default.wrap((0, _koaCompose2.default)(this.paths.get(name)));
    },

    routes: function routes() {
        var self = this;
        /**
         * @this {Packet}
         * */
        return function* (next) {
            if (this.method && self.has(this.method)) {
                yield self.compose(this.method).call(this);
            }
            yield next;
        };
    }

};