'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = require('lodash');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @typedef {object} JSON-RPC~Request
 * @property {string} method - имя метода
 * @property {*} params - параметры запроса
 * @property {number} id - идентификатор запроса
 * */

var Packet = function () {
    /**
     * @constructor
     * @param {JSON-RPC~Request} body - объект запроса
     * */

    function Packet(body) {
        _classCallCheck(this, Packet);

        this.body = body;
    }

    _createClass(Packet, [{
        key: 'getMessage',
        value: function getMessage() {
            return this.body;
        }
    }, {
        key: 'setResolve',
        value: function setResolve(resolve, reject) {
            this.resolve = (0, _lodash.isFunction)(resolve) ? resolve : null;
            this.reject = (0, _lodash.isFunction)(reject) ? reject : null;
        }
    }, {
        key: 'resolve',
        value: function resolve(data) {
            if ((0, _lodash.isObject)(data)) {
                if (data.error) {
                    if (this.reject) {
                        this.reject(data);
                        return;
                    }
                }

                if (this.resolve) {
                    this.resolve(data.result, this);
                }
            }
        }
    }]);

    return Packet;
}();

exports.default = Packet;