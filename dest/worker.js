'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _isFunction = require('lodash/isFunction');

var _isFunction2 = _interopRequireDefault(_isFunction);

var _isObject = require('lodash/isObject');

var _isObject2 = _interopRequireDefault(_isObject);

var _isString = require('lodash/isString');

var _isString2 = _interopRequireDefault(_isString);

var _each = require('lodash/each');

var _each2 = _interopRequireDefault(_each);

var _defer = require('lodash/defer');

var _defer2 = _interopRequireDefault(_defer);

var _pool = require('./pool.js');

var _pool2 = _interopRequireDefault(_pool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @typedef {object} JSON-RPC~Event
 * @property {string} method
 * @property {string} params
 * */

var Worker = function () {
    /**
     * @contructor
     * @param {string} url - урла до файла с логикой для воркера
     * @param {boolean} [isShared=true] - если true, то SharedWorker, иначе DedicatedWorker
     * @return {Worker}
     * */

    function Worker(url) {
        var _this = this;

        var isShared = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];

        _classCallCheck(this, Worker);

        if (url) {
            this.eventChannels = new Map();
            this.eventChannels.set('connect', []);
            this.eventChannels.set('disconnect', []);
            this.eventChannels.set('error', []);

            if ((0, _isString2.default)(url)) {
                this.worker = new window.SharedWorker(url);
            } else if ((0, _isFunction2.default)(url)) {
                this.worker = new url();
            }

            this.port = this.worker.port;

            this.worker.onerror = this.resolveEvent(this, { method: 'error' });

            this.port.onmessage = function (event) {
                var data = event.data;
                if (data.id) {
                    _pool2.default.resolve(data);
                } else {
                    _this.resolveEvent(data);
                }
            };

            this.port.start();
        } else {
            throw new Error('Url is required');
        }
    }

    _createClass(Worker, [{
        key: 'disconnect',
        value: function disconnect() {
            return this.send('close');
        }
    }, {
        key: 'send',
        value: function send(method, data) {
            var _this2 = this;

            return new Promise(function (resolve, reject) {
                var packet = _pool2.default.createPacket(method, data);
                packet.setResolve(resolve, reject);
                _this2.port.postMessage(packet.getMessage());
            });
        }

        /**
         * @param {JSON-RPC~Event} data
         * */

    }, {
        key: 'resolveEvent',
        value: function resolveEvent(data) {
            if ((0, _isObject2.default)(data)) {
                var method = data.method;
                if (this.eventChannels.has(method)) {
                    var params = data.params;
                    (0, _each2.default)(this.eventChannels.get(method), function (cb) {
                        (0, _defer2.default)(cb, params);
                    });
                }
            }
        }
    }, {
        key: 'subscribe',
        value: function subscribe(method, cb) {
            var cbs = [];
            if (this.eventChannels.has(method)) {
                cbs = this.eventChannels.get(method);
            } else {
                this.eventChannels.set(method, cbs);
            }
            cbs.push(cb);
            return this;
        }

        /**
         * @param {function} cb - функция которая будет вызвана после успешного запуска или подключению к воркеру
         * @return {Worker} this
         * */

    }, {
        key: 'onConnect',
        value: function onConnect(cb) {
            if ((0, _isFunction2.default)(cb)) {
                this.eventChannels.get('connect').push(cb);
            }
            return this;
        }

        /**
         * @param {function} cb - функция которая будет вызвана после успешного выключения или отключения от воркера
         * @return {Worker} this
         * */

    }, {
        key: 'onDisconnect',
        value: function onDisconnect(cb) {
            if ((0, _isFunction2.default)(cb)) {
                this.eventChannels.get('disconnect').push(cb);
            }
            return this;
        }

        /**
         * @param {function} cb - функция которая будет вызвана после возникновения ошибки на воркере
         * @return {Worker} this
         * */

    }, {
        key: 'onError',
        value: function onError(cb) {
            if ((0, _isFunction2.default)(cb)) {
                this.eventChannels.get('error').push(cb);
            }
            return this;
        }
    }]);

    return Worker;
}();

exports.default = Worker;