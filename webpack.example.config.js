var path = require("path");
var webpack = require("webpack");

const NODE_ENV = process.env.NODE_ENV || 'development';

const isDev = NODE_ENV == 'development';

module.exports = {
    entry: {
        app: './example/index.js',
        worker: ['babel-polyfill', './example/worker.js'],
    },
    include: [path.resolve(__dirname, './example')],
    output: {
        path: path.resolve(__dirname, './example'),
        filename:'[name].build.js'
    },
    resolve: {
        extensions: ['', '.js'],
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /(node_modules)/,
                loader: 'babel',
                query: {
                    presets: ['es2015', "stage-0"]
                }
            }
        ],
        noParse: []
    },

    watch: isDev,
    cache: true,
    watchOptions: {
        aggregateTimeout: 200,
    },
   /* devtool: isDev ? "inline-source-map" : null,*/
    resolveModules: {
        modulesDirectories: ['node_modules'],
    },
    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV),
            isDev: NODE_ENV === 'development'
        })
    ]
};