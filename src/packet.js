import {isFunction, isObject} from 'lodash';

/**
 * @typedef {object} JSON-RPC~Request
 * @property {string} method - имя метода
 * @property {*} params - параметры запроса
 * @property {number} id - идентификатор запроса
 * */
export default class Packet {
    /**
     * @constructor
     * @param {JSON-RPC~Request} body - объект запроса
     * */
    constructor(body) {
        this.body = body;
    }

    getMessage() {
        return this.body;
    }

    setResolve(resolve, reject) {
        this.resolve = isFunction(resolve) ? resolve : null;
        this.reject = isFunction(reject) ? reject : null;
    }

    resolve(data) {
        if(isObject(data)) {
            if(data.error) {
                if(this.reject) {
                    this.reject(data);
                    return;
                }
            }

            if(this.resolve) {
                this.resolve(data.result, this);
            }
        }
    }
}