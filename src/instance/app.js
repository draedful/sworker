import Middleware from './middleware.js';
import Ports from './port/port_pool.js';
import isFunction from 'lodash/isFunction';
import compose from 'koa-compose';
import co from 'co';

export default {
    middlewares:[],
    use(gen) {
        if(isFunction(gen)) {
            this.middlewares.push(gen);
        }
        return this;
    },
    compose() {
        return co.wrap(compose(this.middlewares))
    }
};