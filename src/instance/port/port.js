import App from '../app.js';
import Packet from './packet.js';
import Pool from './port_pool.js';

export default class Port {
    constructor(port) {
        console.log(port);
        this.port = port;
        this.port.onmessage = (message) => {
            var packet = new Packet(message.data);

            packet.port = this;

            App.compose().call(packet).then(() => {
                this.post(packet.getResponse());
            }).catch((err) => {
                debugger;
                packet.setError(err);
                this.post(packet.getResponse());
            });
        };
    }

    destroy() {
        Pool.remove(this);
    }

    post(message) {
        this.port.postMessage(message);
    }
}