import Port from './port';

const PortsPool = {

    get length() {
        return this.ports.size
    },

    /**
     * @type {Map}
     * */
    ports: new Map(),

    /**
     * @param {MessagePort} port
     * @return {Port} - инстанс порта
     * */
    add(port) {
        var portInstance = new Port(port);
        this.ports.set(port, portInstance);
        return portInstance;
    },

    broadcast(method, data) {
        if(this.length) {
            var ports = Array.from(this.ports.values()),
                message = {
                    method: method,
                    data: data
                }
            each(ports, function(port) {
                port.post(message);
            })
        }
    }

};

self.onconnect = function(e) {
    var port = PortsPool.add(e.ports[0]);
    port.post({method:'connect'})
};

export default PortsPool;

