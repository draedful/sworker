import {isObject, merge} from 'lodash';

/**
 * @typedef {object} JSON-RPC~Response
 * @property {*} result - ответ
 * @property {*} error - ошибка произошедшая при запросе
 * @property {number} id - идентификатор запроса, выданный при инициализации запроса
 * */

export default class Packet {
    constructor(message) {
        this.response = {};
        if(isObject(message)) {
            this.body = message.data;
            this.method = message.method;
            this.id = message.id;
        }
    }

    set(name, value) {
        if(isObject(name)) {
            merge(this.response, name);
        } else {
            this.response[name] = value;
        }

        return this;
    }

    get(name) {
        return this.response[name];
    }

    /**
     * @return
     * */
    getResponse() {
        var response =  {
            id: this.id
        };

        if(this.error) {
            response.error = this.error;
        } else {
            response.result = this.response;
        }

        return JSON.parse(JSON.stringify(response));
    }

    setError(err) {
        this.error = err;
    }

}