import App from './app.js';
import Pool from './port/port_pool.js';

App.use('close', function* () {
    if(!Pool.length) {
        self.close();
    }
});