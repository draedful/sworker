import {each} from  'lodash';
import compose from 'koa-compose';
import co from 'co';

export default class Middleware {
    constructor(middlewares) {
        this.middlewars = [...middlewares];
    }

    getMiddleware() {
        return this.middlewars;
    }

    call(ctx) {
        if(this.middlewars.length) {
            return co.wrap(compose(this.middlewars)).call(ctx);
        }
        return Promise.reject('Method not found');
    }
}