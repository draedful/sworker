import isFunction from 'lodash/isFunction';
import isObject from 'lodash/isObject';
import isString from 'lodash/isString';
import each from 'lodash/each';
import defer from 'lodash/defer';

import PacketPool from './pool.js';

/**
 * @typedef {object} JSON-RPC~Event
 * @property {string} method
 * @property {string} params
 * */
export default class Worker {
    /**
     * @contructor
     * @param {string} url - урла до файла с логикой для воркера
     * @param {boolean} [isShared=true] - если true, то SharedWorker, иначе DedicatedWorker
     * @return {Worker}
     * */
    constructor(url, isShared = true) {
        if(url) {
            this.eventChannels = new Map();
            this.eventChannels.set('connect', []);
            this.eventChannels.set('disconnect', []);
            this.eventChannels.set('error', []);

            if(isString(url)) {
                this.worker = new (window.SharedWorker)(url);
            } else if(isFunction(url)) {
                this.worker = new url();
            }

            this.port = this.worker.port;

            this.worker.onerror = this.resolveEvent(this, {method: 'error'});

            this.port.onmessage = (event) => {
                var data = event.data;
                if(data.id) {
                    PacketPool.resolve(data);
                } else {
                    this.resolveEvent(data);
                }
            }

            this.port.start();

        } else {
            throw new Error('Url is required');
        }
    }

    disconnect() {
        return this.send('close');
    }

    send(method, data) {
        return new Promise((resolve, reject) => {
            var packet =  PacketPool.createPacket(method, data);
            packet.setResolve(resolve, reject);
            this.port.postMessage(packet.getMessage())
        })
    }

    /**
     * @param {JSON-RPC~Event} data
     * */
    resolveEvent(data) {
        if(isObject(data)) {
            var method = data.method;
            if(this.eventChannels.has(method)) {
                var params = data.params;
                each(this.eventChannels.get(method), (cb) => {
                    defer(cb, params);
                })
            }
        }
    }

    subscribe(method, cb) {
        var cbs = [];
        if(this.eventChannels.has(method)) {
            cbs = this.eventChannels.get(method);
        } else {
            this.eventChannels.set(method, cbs);
        }
        cbs.push(cb);
        return this;
    }

    /**
     * @param {function} cb - функция которая будет вызвана после успешного запуска или подключению к воркеру
     * @return {Worker} this
     * */
    onConnect(cb) {
        if(isFunction(cb)) {
            this.eventChannels.get('connect').push(cb);
        }
        return this;
    }

    /**
     * @param {function} cb - функция которая будет вызвана после успешного выключения или отключения от воркера
     * @return {Worker} this
     * */
    onDisconnect(cb) {
        if(isFunction(cb)) {
            this.eventChannels.get('disconnect').push(cb);
        }
        return this;
    }

    /**
     * @param {function} cb - функция которая будет вызвана после возникновения ошибки на воркере
     * @return {Worker} this
     * */
    onError(cb) {
        if(isFunction(cb)) {
            this.eventChannels.get('error').push(cb);
        }
        return this;
    }



}