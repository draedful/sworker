import Packet from './packet.js';
import isObject from 'lodash/isObject';

export default {
    pool: new Map(),
    index: 1,

    createPacket(method, data) {
        var currentIndex = this.index++,
            /**
             * @type {JSON-RPC~Request}
             * */
            packetData = {
                method: method,
                params: data,
                id: currentIndex
            };
        var packet = new Packet(packetData);
        this.pool.set(currentIndex, packet);
        return packet;
    },

    resolve(data) {
        if(isObject(data)) {
            if(data.id) {
                var packet = this.pool.get(data.id);
                if(packet && packet instanceof Packet) {
                    packet.resolve(data);
                    this.pool.delete(data.id);
                    return true;
                }
            }
        }
        return false;
    }
}