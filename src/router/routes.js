import isFunction from 'lodash/isFunction';
import filter from 'lodash/filter';
import compose from 'koa-compose';
import co from 'co';

export default {
    paths: new Map(),
    use(name, ...cbs) {
        if(name && cbs.length) {
            if(this.paths.has(name)) {
                var middlewares = this.paths.get(name);
                Array.prototype.concat.call(middlewares, filter(cbs, isFunction));
            } else {
                this.paths.set(name, cbs);
            }
            // слияние массива без потери ссылки

        }
        return this;
    },
    has(name) {
        return this.paths.has(name);
    },
    compose(name) {
          return co.wrap(compose(this.paths.get(name)))
    },
    routes: function () {
        var self = this;
        /**
         * @this {Packet}
         * */
        return function* (next) {
            if(this.method && self.has(this.method)) {
                yield self.compose(this.method).call(this);
            }
            yield next;
        }
    }

}