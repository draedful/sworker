var path = require("path");

module.exports = {
    entry: path.resolve(__dirname, 'src/index.js')/*{
        app: './src/worker.js',
        worker: './src/instance/app.js',
    }*/,
    include: [path.resolve(__dirname, 'src')],
    output: {
        /*path: path.resolve(__dirname, './build'),*/
        filename:'index.js',
        libraryTarget: 'umd',
        library: 'sworker',
        umdNamedDefine: true
    },
    resolve: {
        extensions: ['', '.js'],
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /(node_modules)/,
                loaders: ['babel?presets[]=es2015'],
            }
        ],
        noParse: []
    },
    resolveModules: {
        modulesDirectories: ['node_modules'],
    }
};