import "babel-polyfill";
import {App} from '../src/index.js';
import logger from './logger.js';
import {router} from '../src/router/index.js';


router.use('ping', function*(next) {
    this.set('pong', 123);
    yield next;
});

router.use('test', function*(next) {
    this.set('pong', this.body ? this.body : 'body is not defined');
    yield next;
});

App
    .use(logger())
    .use(router.routes());