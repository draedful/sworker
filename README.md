
## Installation

```bash
npm install --save sworker
```

## Examples

### main

```javascript
    
    import {Worker} from 'sworker';
    
    var sw = new Worker('worker.js'); // create shared worker
    
    sw.onConnect(() => {
        sw.send('test').then(function(e) {
            console.log(e) // { result: 'data' }
        }).catch(function() {
            debugger;
        });
        
        sw.send('error').then(function(e) {
            console.log(e) 
        }).catch(function(e) {
            console.error(e); // Method not found
        })
        
    });
    
    
    sw.onError(() => {
        debugger;
    });
   

```

### worker

```javascript

    import {App} from 'sworker';
    
    App.use('test', function* (next) {
        yield Promise.resolve(1);
        
        var res = yield {
          1: Promise.resolve(1),
          2: Promise.resolve(2),
        };
        
        console.log(res); // => { 1: 1, 2: 2 }
        
        yield next;
        console.log('action after');
    }, function* (next) {
    
        // yielding function
        yield function () {
            
        };
        
        // yielding generator
        yield function* () {
                          
        };
        
        // async yielding generator
        yield [function(){
        
        }, function* (){
        
        }];
        
        this.set('result', 'data'); // set response
        
        yield next;
    });

```